package com.aaapp.aaapp;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.io.File;
import java.util.List;

public class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.ImageViewHolder>{

    private final int THUMBSIZE = 350;

    private List<String> imagesList;

    private View.OnClickListener onItemClickListener;

    ImageAdapter(List<String> list){
        imagesList = list;
    }

    void setOnItemClickListener(View.OnClickListener clickListener) {
        onItemClickListener = clickListener;
    }

    /**
     * ViewHolder custom. La RecyclerView crea (attraverso l' Adapter) un ViewHolder per ogni oggetto
     * nella porzione della lista visibile a schermo
     */
    class ImageViewHolder extends RecyclerView.ViewHolder {
        private ImageView imageView;

        ImageViewHolder(@NonNull View itemView) {
            super(itemView);
            itemView.setTag(this);
            itemView.setOnClickListener(onItemClickListener);
            imageView = itemView.findViewById(R.id.image_layout);
        }


        ImageView getImageView() {
            return imageView;
        }


    }

    @Override
    public ImageViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        return new ImageViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.images_layout, parent, false));
    }

    /**
     * Assegna al ViewHolder passatogli la Bitmap nella posizione i della lista
     *
     * @param imageViewHolder ViewHolder a cui bisogna assegnare un immagine
     * @param i               indice nella lista
     */
    @Override
    public void onBindViewHolder(@NonNull ImageViewHolder imageViewHolder, int i) {
        Uri imagePath = Uri.parse(imagesList.get(i));
        File imageFile = new File(imagePath.getPath());
        Bitmap imageBitmap = BitmapFactory.decodeFile(imageFile.getAbsolutePath());

        // Riduco le dimensioni dell' immagine per rendere fluida la RecyclerView
        imageViewHolder.getImageView().setImageBitmap(getResizedBitmap(imageBitmap, THUMBSIZE));
        imageViewHolder.getImageView().setScaleType(ImageView.ScaleType.CENTER_CROP);
    }

    /**
     * Ridimensiona la Bitmap
     * @param image Bitmap da ridimensionare
     * @param maxSize dimensione massima che può avere l' immagine
     * @return Bitmap ridimensionata
     */
    Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        try {
            int width = image.getWidth();
            int height = image.getHeight();

            float bitmapRatio = (float) width / (float) height;
            if (bitmapRatio > 1) {
                width = maxSize;
                height = (int) (width / bitmapRatio);
            } else {
                height = maxSize;
                width = (int) (height * bitmapRatio);
            }
            return Bitmap.createScaledBitmap(image, width, height, true);
        } catch(NullPointerException e){
            e.printStackTrace();
            return null;
        } catch (IllegalArgumentException e){
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public int getItemCount() {
        return imagesList.size();
    }
}