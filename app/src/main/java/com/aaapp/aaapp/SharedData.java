package com.aaapp.aaapp;

/**
 * Insieme di costanti utili a varie classi
 */
class SharedData {
    static final int TF_OD_API_INPUT_SIZE = 300;
    static final boolean TF_OD_API_IS_QUANTIZED = true;
    static final String TF_OD_API_MODEL_FILE = "detect.tflite";
    static final String TF_OD_API_LABELS_FILE = "file:///android_asset/labelmap.txt";

    static final int EXTERNAL_STORAGE_REQ_CODE = 100;
    static final String ACTION_SCAN = "com.aaapp.aaapp.action.SCAN";
    static final String ACTION_RESUME = "com.aaapp.aaapp.action.RESUME";
    static final String PATH_PARAM = "com.aaapp.aaapp.extra.PATH";
    static final String BROADCAST_ACTION = "com.aaapp.aaapp.BROADCAST";
    static final String BROADCAST_STATUS_ID = "com.aaapp.aaapp.STATUS";
    static final String NOTIFICATION_CHANNEL_ID = "com.aaapp.aaapp.CHANNEL";
    static final int NOTIFICATION_ID = 10;
}
