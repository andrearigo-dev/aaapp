package com.aaapp.aaapp;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.SystemClock;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.aaapp.aaapp.tflite.Classifier;

import java.io.File;
import java.util.ArrayList;
import java.util.Objects;

import static com.aaapp.aaapp.SharedData.ACTION_RESUME;
import static com.aaapp.aaapp.SharedData.ACTION_SCAN;
import static com.aaapp.aaapp.SharedData.BROADCAST_ACTION;
import static com.aaapp.aaapp.SharedData.BROADCAST_STATUS_ID;
import static com.aaapp.aaapp.SharedData.PATH_PARAM;

public class DetectorActivity extends AppCompatActivity{

    private float totalSize = 0;
    private float mProgressStatus = 0;

    private boolean startProgressBar = false;
    private boolean doubleBackToExitPressedOnce = false;

    private String path;

    private ArrayList<String> labelElement;
    private ArrayList<String> pathReco;

    private BroadcastReceiver broadcastReceiver;

    private Button buttonPause;

    private Handler mHandler = new Handler();

    private RecyclerView recyclerView;

    private TextView naf; //Nothing Already Found

    private ProgressBar mProgressBar;

    private boolean isBound;
    private ScanService service;

    // Connessione al service
    private ServiceConnection connection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder binder) {
            isBound = true;
            service = ((ScanService.LocalServiceBinder) binder).getService();

            run();

            Log.e("TEST", "Bound");
        }

        public void onServiceDisconnected(ComponentName className) {
            isBound = false;
        }
    };

    //Listener necessario per l'onClick su gli elementi della recyclerview
    private View.OnClickListener onItemClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            RecyclerView.ViewHolder viewHolder = (RecyclerView.ViewHolder) view.getTag();
            int position = viewHolder.getAdapterPosition();
            Context context = DetectorActivity.this;

            File f = new File(pathReco.get(position));
            if (f.isFile()) {
                MediaScannerConnection.scanFile(context, new String[]{f.toString()}, null, new MediaScannerConnection.OnScanCompletedListener() {
                    @Override
                    public void onScanCompleted(String path, Uri uri) {

                        // Esegue un intent per richiamare applicazioni che possano visualizzare immagini
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setDataAndType(uri, "image/*");
                        DetectorActivity.this.startActivity(intent);
                    }
                });
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if((savedInstanceState != null)
                &&(savedInstanceState.getStringArrayList("pathReco")!=null)){
            pathReco = savedInstanceState.getStringArrayList("pathReco");
        } else{
            pathReco = new ArrayList<>();
        }

        setContentView(R.layout.activity_detector);

        Intent intent = getIntent();

        //Riceve la lista di label dall'activity precedente
        labelElement = intent.getStringArrayListExtra("labelElement");

        //Riceve il percorso di ricerca dall'activity precedente
        path = intent.getStringExtra("path");

        mProgressBar = findViewById(R.id.ProgressBar);
        naf = findViewById(R.id.nothing_already_found);
        recyclerView = findViewById(R.id.recycler_view);
        buttonPause = findViewById(R.id.buttonStop);

        //Crea l'ImageAdapter necessario per la visualizzazione dei risultati
        ImageAdapter mRecyclerViewAdapter = new ImageAdapter(pathReco);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(mRecyclerViewAdapter);
        mRecyclerViewAdapter.setOnItemClickListener(onItemClickListener);

        onPausePressed(buttonPause);

        // Bind del service
        Intent scanIntent = new Intent(this, ScanService.class);
        bindService(scanIntent, connection,
                Context.BIND_AUTO_CREATE);
    }




    /**
     * Invia il lavoro al service passandogli tutti i dati necessari per classificare le immagini e
     * riceve le immagini che sono state classificate tramite Local Broadcast
     */
    protected void run() {
        ArrayList<String> paths = new ArrayList<>();
        if(path.equals("")) {
             paths.add(getString(R.string.basicPath));
        }else{
            paths.add(path);
        }

        //Invia il lavoro il service
        Intent intent = new Intent(this, ScanService.class);
        intent.setAction(ACTION_SCAN);
        intent.putStringArrayListExtra(PATH_PARAM, paths);
        startService(intent);

        //Riceve le immagini classificate dal service
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String msg = intent.getStringExtra(BROADCAST_STATUS_ID);
                if (recyclerView.getChildCount() > 0) {
                    naf.setVisibility(View.GONE);
                    naf.setVisibility(View.INVISIBLE);
                }
                show(Objects.requireNonNull(service.getImg(msg)));
                if (!startProgressBar) {
                    startProgressBar = true;
                    startProgressBarStatus();
                }
            }
        };
        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver, new IntentFilter(BROADCAST_ACTION));
    }



    /**
     * Avvia la barra di progresso
     */
    private void startProgressBarStatus() {
        totalSize = service.getMax();
        mProgressStatus = (service.getProgress() * 100) / totalSize;
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (service.getProgress() < totalSize) {
                    mProgressStatus = (service.getProgress() * 100) / totalSize;
                    SystemClock.sleep(100);
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            mProgressBar.setProgress((int)mProgressStatus);
                        }
                    });
                }
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (service.getProgress() == totalSize) {
                            hideProgressBarAndButtonPause();
                            checkIfEmpty();
                            showToastMessage(getString(R.string.txt_done));
                        }
                    }
                });
            }
        }).start();
    }

    /**
     * Memorizza il percorso delle immagini classificate controllando prima che le classi
     * individuate dal classificatore appartengano alle scelte dell'utente
     *
     * Notifica, dopo i dovuti controlli, che la lista che usa la recyclerView è stata aggiornata
     *
     * @param classifiedImage immagine classificata dal modello
     */
    private void show(ClassifiedImage classifiedImage) {
        ArrayList<String> tempLabel = new ArrayList<>(0);
        for (Classifier.Recognition rec : classifiedImage.getData()) {
            if(labelElement.contains(rec.getTitle())){
                if(!tempLabel.contains(rec.getTitle())){
                    tempLabel.add(rec.getTitle());
                }
            }
        }
        // Verifica che le scelte dell'utente siano compatibili con i risultati del modello
        if(labelElement.size() == tempLabel.size()){
            pathReco.add(classifiedImage.getPath());
            Objects.requireNonNull(recyclerView.getAdapter()).notifyDataSetChanged();
        }
    }


    /**
     * Listener per il pulsante pause (usato per bloccare la scansione)
     * @param button button STOP clicked
     */
    protected void onPausePressed(final Button button){
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                if (isBound) {
                    if (service.isBusy()) { //va in pausa
                        service.pause();
                        showToastMessage(getString(R.string.txt_pause));
                        buttonPause.setText(getString(R.string.txt_resume));
                        checkIfEmpty();
                    } else { //riprendi
                        startService(new Intent(DetectorActivity.this, ScanService.class).setAction(ACTION_RESUME));
                        showToastMessage(getString(R.string.txt_resume));
                        buttonPause.setText(getString(R.string.txt_pause));
                    }
                }
            }
        });
    }

    /**
     * Imposta in non visibile e slegato dal layout la progressbar e il pulsante pause
     * Reimposta il vincolo 'top' della recyclerView
     */
    protected void hideProgressBarAndButtonPause(){
        mProgressBar.setVisibility(View.GONE);
        buttonPause.setVisibility(View.GONE);
        mProgressBar.setVisibility(View.INVISIBLE);
        buttonPause.setVisibility(View.INVISIBLE);
        ConstraintSet constraintSet = new ConstraintSet();
        ConstraintLayout constraintLayout = findViewById(R.id.ConstraintLayoutDetect);
        constraintSet.clone(constraintLayout);
        constraintSet.connect(R.id.recycler_view, ConstraintSet.TOP, ConstraintSet.PARENT_ID, ConstraintSet.TOP);
        constraintSet.applyTo(constraintLayout);
    }

    /**
     * Verifica se sono state trovate immagini
     * controllando il numero di figli della recyclerView
     * nel caso TRUE stampa a schermo che nulla è stato trovato
     */
    protected void checkIfEmpty(){
        if(recyclerView.getChildCount()==0){
            naf.setText(R.string.nothing_found);
        }
    }

    /**
     * Mostra i toast message
     *
     * @param message messaggio da mostrare
     */
    private void showToastMessage(String message) {
        Toast toast = Toast.makeText(getApplicationContext(),
                message,
                Toast.LENGTH_SHORT);
        toast.show();
    }

    /**
     * Implementato il doppio click sul tasto back per uscire dall'activity
     * per limitare i tocchi involontari durante la scansione
     */
    @Override
    public void onBackPressed() {
        if(doubleBackToExitPressedOnce) {
            super.onBackPressed();
            service.pause();
            finish();
        }
        this.doubleBackToExitPressedOnce = true;
        showToastMessage(getString(R.string.double_click_back));
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }

    @Override
    protected void onSaveInstanceState(Bundle state) {
        super.onSaveInstanceState(state);
        //Salva la variabile contente i percorsi delle immagini classificate
        state.putStringArrayList("pathReco",pathReco);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Bundle bundle = new Bundle();
        onSaveInstanceState(bundle);
    }

    @Override
    protected void onDestroy() {
        //Scollega il broadcastReceiver
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver);
        if (isBound)
            unbindService(connection);
        super.onDestroy();
    }
}
