package com.aaapp.aaapp;

import android.app.IntentService;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Intent;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import java.util.List;

import static com.aaapp.aaapp.SharedData.ACTION_RESUME;
import static com.aaapp.aaapp.SharedData.ACTION_SCAN;
import static com.aaapp.aaapp.SharedData.BROADCAST_ACTION;
import static com.aaapp.aaapp.SharedData.BROADCAST_STATUS_ID;
import static com.aaapp.aaapp.SharedData.NOTIFICATION_CHANNEL_ID;
import static com.aaapp.aaapp.SharedData.NOTIFICATION_ID;
import static com.aaapp.aaapp.SharedData.PATH_PARAM;
import static com.aaapp.aaapp.SharedData.TF_OD_API_INPUT_SIZE;
import static com.aaapp.aaapp.SharedData.TF_OD_API_IS_QUANTIZED;
import static com.aaapp.aaapp.SharedData.TF_OD_API_LABELS_FILE;
import static com.aaapp.aaapp.SharedData.TF_OD_API_MODEL_FILE;


public class ScanService extends IntentService {

    private ObjDetector detector;
    private ArchiveHandler archiveHandler;

    private NotificationCompat.Builder notificationBuilder;
    private NotificationManagerCompat notificationManager;

    //Flag utilizzato per interrompere la scansione in corso
    private static boolean canScan = true;

    public ScanService() {
        super("ScanService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_SCAN.equals(action)) {
                final List<String> paths = intent.getStringArrayListExtra(PATH_PARAM);
                notifyScanState();
                prepareScan(paths);
                scan();
            } else if (ACTION_RESUME.equals(action)) {
                canScan = true;
                scan();
            }
        }
    }

    /**
     * Inizializza il classificatore, il canale delle notifiche, la barra di progresso nella
     * notifica e cerca le immagini nei percorsi ricevuti
     *
     * @param paths Lista di percorsi in cui cercare immagini
     */
    private void prepareScan(List<String> paths) {
        /*
         * Rende la barra di progresso indeterminata perché non possiamo conoscere
         * a priori il numero massimo di file da classificare
         */
        notifyScanState(0, 0, true);
        archiveHandler = new ArchiveHandler(paths);

        // Cerca tutti i file da classificare
        archiveHandler.explore();

        // Inizializza il classificatore
        detector = new ObjDetector(
                getAssets(),
                TF_OD_API_MODEL_FILE,
                TF_OD_API_LABELS_FILE,
                TF_OD_API_INPUT_SIZE,
                TF_OD_API_IS_QUANTIZED,
                0.75f,
                archiveHandler
        );

        // Inizializza la barra di prograsso con il numero totale di immagini da scansionare
        notifyScanState(archiveHandler.getImages().size(), 0, false);
    }

    /**
     * Invia broadcast che sarà poi ricevuto dall' activity
     * @param msg Messaggio da inviare
     */
    private void sendToActivity(String msg) {
        Intent localIntent = new Intent(BROADCAST_ACTION).putExtra(BROADCAST_STATUS_ID, msg);
        LocalBroadcastManager.getInstance(this).sendBroadcast(localIntent);
    }

    private void scan() {
        canScan = true;

        /*
         * Passa un'immagine alla volta al classificatore, invia i risulati all'activity
         * e aggiorna il progresso sulla barra nella notifica
         */
        while (canScan && detector.hasNext()) {
            sendToActivity(detector.detectNext());
            notifyScanState(archiveHandler.getImages().size(), detector.getProgress(), false);
        }

        /*
         * Rimuove la barra di progresso dalla notifica e la sostituisce con un messaggio che avvisa
         * l' utente che la scansione è terminata. Lo fa solo se la scansione non è stata interrotta
         * dall' utente stesso.
         */
        if (canScan) {
            notifyCompletion();
            canScan = false;
        }

    }

    /**
     * Aggiorna la barra di progresso
     * @param max Numero totale delle immagini da classificare
     * @param progress Ultima immagine classificata
     * @param indeterminate Indica se la barra deve essere indeterminata
     */
    private void notifyScanState(int max, int progress, boolean indeterminate) {
        notificationBuilder.setProgress(max, progress, indeterminate)
                .setContentText(getString(R.string.notification_progress) + ": " + progress + " / " + max);
        notificationManager.notify(NOTIFICATION_ID, notificationBuilder.build());

        //Log.e(TAG, "Notification: " + progress + "/" + max);
    }

    /**
     * Rimuove la barra di progresso e avvisa l' utente della fine della scansione
     */
    private void notifyCompletion() {
        // Notifica il completamento del lavoro e elimina la barra di progresso
        notificationBuilder.setProgress(0, 0, false)
                .setContentText(getString(R.string.scan_completed_notification));
        notificationManager.notify(NOTIFICATION_ID, notificationBuilder.build());
    }

    /**
     * Crea il canale per le notifiche e la barra di progresso
     */
    private void notifyScanState() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "AAApp";
            String description = "";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, name, importance);
            channel.setDescription(description);

            /*
             * Registra il canale nel sistema
             */
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }

        notificationBuilder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID)
                .setContentTitle(getString(R.string.notification_title))
                .setSmallIcon(R.drawable.ic_launcher_foreground)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)

                // Inizializza barra di progresso
                .setProgress(100, 0, false)

                // La notifica emettera suono/vibrazione solo la prima volta anziché ad ogni aggiornamento
                .setOnlyAlertOnce(true);

        notificationManager = NotificationManagerCompat.from(this);
    }

    void pause() {
        canScan = false;
    }

    /**
     * Indica se c'è una scansione in corso
     * @return true se c'è una scansione in corso, false altrimenti
     */
    boolean isBusy() {
        return canScan;
    }

    int getMax() {
        return archiveHandler.getImages().size();
    }

    int getProgress() {
        return detector.getProgress();
    }

    /**
     * Ritorna un' immagine classificata dato il suo percorso
     * @param path Percorso dell' immagine classificata
     * @return Immagine classificata e risultati della classificazione
     */
    ClassifiedImage getImg(String path) {
        return detector.getClassifiedImages().get(path);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return new LocalServiceBinder();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (detector != null)
            detector.close();
        String TAG = "SS";
        Log.e(TAG, "Service stopped");
    }

    class LocalServiceBinder extends Binder {
        ScanService getService() {
            return ScanService.this;
        }
    }
}
