package com.aaapp.aaapp;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.aaapp.aaapp.tflite.Classifier;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

class ClassifiedImage implements Serializable {

    private final String path;

    private final List<Classifier.Recognition> data;

    ClassifiedImage(String path) {
        this.path = path;
        data = new ArrayList<>();
    }

    ClassifiedImage(String path, List<Classifier.Recognition> results) {
        this.path = path;
        data = results;
    }

    String getPath() {
        return path;
    }

    /**
     * Restituisce l'immagine sotto forma di oggetto Bitmap
     * @return Bitmap dell' immagine
    */
    private Bitmap getBitmap() {
        return BitmapFactory.decodeFile(path);
    }

    /**
     * Restituisce l'immagine ridimensionata
     * @return Bitmap dell' immagine ridimensionata
    */
    Bitmap getScaled(int scaleSize) {
        try {
            return Bitmap.createScaledBitmap(getBitmap(), scaleSize, scaleSize, true);
        }catch (NullPointerException e){
            Log.e("[ERROR]", "IMMAGINE NON VALIDA");
            return Bitmap.createBitmap(scaleSize,scaleSize,Bitmap.Config.ARGB_8888);
        }
    }

    /**
     * Restituisce i risultati ottenuti dalla rete
     *
     * @return lista di oggetti riconosciuti dalla rete
     */
    List<Classifier.Recognition> getData() {
        return data;
    }
}
