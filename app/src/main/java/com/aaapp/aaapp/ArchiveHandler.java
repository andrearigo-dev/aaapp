package com.aaapp.aaapp;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

class ArchiveHandler {

    private boolean res;
    private final List<String> paths;
    private final String[] okFileExtensions =  new String[] {"jpg", "png","jpeg"};
    private final ArrayList<ClassifiedImage> images;

    ArchiveHandler(List<String> paths) {
        this.paths = paths;
        images = new ArrayList<>();
    }

    ArrayList<ClassifiedImage> getImages() {
        return images;
    }

    /**
     * Verifica se un file è un'immagine
     *
     * @param file file da controllare
     * @return true se il file è un'immagine, false altrimenti
     */
    private boolean isImage(File file){
        res = false;
        for(String extension : okFileExtensions){
            if(file.getName().toLowerCase().endsWith(extension)) {
                res = true;
            }
        }
        return res;
    }

    /**
     * Esplora ricorsivamente le certelle nella lista dei percorsi
     *
     * @param path percorso della cartella di partenza
     */
    private void explore(String path){
        File file = new File(path);
        if (file.isDirectory())
            for (File elem : file.listFiles())
                if (elem.isFile() && isImage(elem))
                    images.add(new ClassifiedImage(elem.getAbsolutePath()));
                else
                    explore(elem.getAbsolutePath());
        else if (file.isFile() && isImage(file))
            images.add(new ClassifiedImage(file.getAbsolutePath()));
    }

    /**
     * Chiama il metodo explore su ogni percorso nella lista
     */
    void explore() {
        for (String path : paths)
            explore(path);
    }
}
