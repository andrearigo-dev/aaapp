package com.aaapp.aaapp;

import android.content.res.AssetManager;
import android.util.Log;

import com.aaapp.aaapp.tflite.Classifier;
import com.aaapp.aaapp.tflite.TFLiteObjectDetectionAPIModel;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

class ObjDetector {

    private final String TAG = "OD";

    private final int inputSize;
    private final float confidenceThreshold;

    private Classifier objDet;
    private final ArchiveHandler archiveHandler;

    private HashMap<String, ClassifiedImage> classifiedImages;

    private int progress;

    ObjDetector(AssetManager assets, String modelFile, String labelsFile, int inputSize,
                boolean isQuantized, float confidenceThreshold, ArchiveHandler archiveHandler) {

        this.inputSize = inputSize;
        this.confidenceThreshold = confidenceThreshold;
        this.archiveHandler = archiveHandler;

        classifiedImages = new HashMap<>();

        progress = 0;

        try {
            objDet = TFLiteObjectDetectionAPIModel.create(
                    assets,
                    modelFile,
                    labelsFile,
                    inputSize,
                    isQuantized
            );
        } catch (final IOException e) {
            e.printStackTrace();
            Log.e(TAG, "Exception initializing classifier!", e);
        }
    }

    HashMap<String, ClassifiedImage> getClassifiedImages() {
        return classifiedImages;
    }

    /**
     * Rimuove le voci sotto la soglia di accuratezza
     *
     * @param modelResults lista di riconoscimenti ottenuti dal classificatore
     * @return a lista dei riconoscimnti sopra la soglia
     */
    private List<Classifier.Recognition> clean(List<Classifier.Recognition> modelResults) {
        ArrayList<Classifier.Recognition> res = new ArrayList<>();
        for (Classifier.Recognition rec : modelResults)
            if (rec.getConfidence() > confidenceThreshold)
                res.add(rec);

        return res;
    }

    /**
     * Passa al modello la prossima immagine se presente
     * @return percorso dell' immagine appena classificata
     */
    String detectNext() {
        String res = "";
        if (hasNext()) {
            ClassifiedImage img = archiveHandler.getImages().get(progress);
            if (!classifiedImages.containsKey(img.getPath()))
                classifiedImages.put(img.getPath(), new ClassifiedImage(
                        img.getPath(),
                        clean(objDet.recognizeImage(img.getScaled(inputSize))))
                );
            res = img.getPath();
            progress++;
        }
        return res;
    }

    /**
     * Indica se ci sono ancora immagini da classificare
     *
     * @return true se ci sono ancora immagini da scansionare, false altrimenti
     */
    boolean hasNext() {
        return progress < archiveHandler.getImages().size();
    }

    int getProgress() {
        return progress;
    }

    void close() {
        objDet.close();
    }
}
