package com.aaapp.aaapp;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import static com.aaapp.aaapp.SharedData.EXTERNAL_STORAGE_REQ_CODE;

public class MainActivity extends Activity {

    private TextView txt_pathShow;
    private String selectedPath;
    private ArrayList<String> labelElements = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        double reSizeHeight = 0.2;
        String filename = "labelmap.txt";

        checkPermission();

        RadioGroup radioGroup = findViewById(R.id.radioGroupFolder);
        Button buttonSelectAll = findViewById(R.id.buttonSelectAll);
        Button buttonDeselectAll = findViewById(R.id.buttonDeselectAll);
        Button buttonScan = findViewById(R.id.buttonScan);
        ScrollView scrollView = findViewById(R.id.scrollViewSelectType);

        selectedPath = getString(R.string.basicPath);

        createCheckboxList();
        setRealPath(radioGroup);
        selectAll(buttonSelectAll);
        deselectAll(buttonDeselectAll);
        clickScan(buttonScan);

        resizeScrollView(reSizeHeight, scrollView);
        readLabelFile(filename);
        createCheckboxList();
    }

    /**
     * Legge il file di label e popola l'ArrayList
     */
    private void readLabelFile(String filename) {
        BufferedReader buffReader;
        String tempString;
        try {
            buffReader = new BufferedReader(new InputStreamReader(getAssets().open(filename)));
            while ((tempString = buffReader.readLine()) != null) {
                labelElements.add(tempString);
            }
            buffReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    /**
     * Esegue il resize della scrollview contenente la lista di checkbox
     */
    private void resizeScrollView(double factor, ScrollView scrollView) {
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        double newHeight = (double) size.y * factor;
        ViewGroup.LayoutParams layoutParams = scrollView.getLayoutParams();
        layoutParams.height = (int) newHeight;
        scrollView.setLayoutParams(layoutParams);
    }

    /**
     * Verifica i permessi
     */
    private void checkPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    EXTERNAL_STORAGE_REQ_CODE);
        }
    }

    /**
     * Seleziona tutte le voci nella lista di checkbox
     *
     * @param button pulsante "seleziona tutto"
     */
    protected void selectAll(final Button button){
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                LinearLayout ll= findViewById(R.id.LinearLayoutSelectType);
                final int childCountCB = ll.getChildCount();
                for(int j=0; j<childCountCB; j++){
                    View element = ll.getChildAt(j);
                    if(element instanceof CheckBox){
                        CheckBox checkBox = (CheckBox) element;
                        checkBox.setChecked(true);
                    }
                }
            }
        });
    }

    /**
     * Deseleziona tutte le voci nella lista di checkbox
     *
     * @param button pulsante "deseleziona tutto"
     */
    protected void deselectAll(final Button button){
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                LinearLayout ll= findViewById(R.id.LinearLayoutSelectType);
                final int childCountCB = ll.getChildCount();
                for(int j=0; j<childCountCB; j++){
                    View element = ll.getChildAt(j);
                    if(element instanceof CheckBox){
                        CheckBox checkBox = (CheckBox) element;
                        checkBox.setChecked(false);
                    }
                }
            }
        });
    }

    /**
     * Seleziona il percorso di ricerca e
     * scrive nella TextView il percorso appena selezionato
     *
     * @param radioGroup radioGroup to select the path to scan
     */
    protected void setRealPath(RadioGroup radioGroup){
        txt_pathShow = findViewById(R.id.textView_selectedFolder);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId){
                    case R.id.radioAllFolders:
                        selectedPath = getString(R.string.basicPath);
                        txt_pathShow.setText(selectedPath);
                        break;
                    case R.id.radioSelectAFolder:
                        //Manda un intent che apre la selezione della cartella
                        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT_TREE);
                        intent.addCategory(Intent.CATEGORY_DEFAULT);
                        startActivityForResult(intent, 9999);
                        break;
                }
            }
        });
    }

    /**
     * Crea la checkbox list a partire dall'ArrayList
     */
    protected void createCheckboxList(){
        LinearLayout constraintLayout = findViewById(R.id.LinearLayoutSelectType);
        LinearLayout.LayoutParams checkBoxParam = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT
        );

        checkBoxParam.setMargins(10,10,10,10);
        checkBoxParam.gravity = Gravity.START;

        for (String s : labelElements) {
            if (!s.equals("???")) {
                CheckBox checkBox = new CheckBox(this);
                //checkBox.setId(s.);
                checkBox.setText(s);
                constraintLayout.addView(checkBox, checkBoxParam);
            }
        }
    }

    /**
     * Verifica che almeno un elemento è stato selezionato nella checkbox list
     * prima di avviare l'activity successiva
     *
     * @param button pulsante SCAN
     */
    public void clickScan(final Button button) {
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<String> labelList = new ArrayList<>();
                // Get all label checked
                LinearLayout linearLayout = findViewById(R.id.LinearLayoutSelectType);
                for (int i=0; i < linearLayout.getChildCount(); i++) {
                    CheckBox checkBox = (CheckBox)linearLayout.getChildAt(i);
                    if(checkBox.isChecked()){
                        Log.d("[INFO]", checkBox.getText().toString());
                        labelList.add(checkBox.getText().toString());
                    }
                }
                Log.e("msg", selectedPath);
                if(!labelList.isEmpty()) {
                    Intent intent = new Intent(MainActivity.this, DetectorActivity.class);
                    intent.putExtra("path", selectedPath);
                    intent.putStringArrayListExtra("labelElement", labelList);
                    startActivity(intent);
                }else{
                    showToastMessage(getString(R.string.select_at_least_type));

                }
            }
        });
    }

    /**
     * Mostra i toast message
     *
     * @param message messaggio da mostrare
     */
    private void showToastMessage(String message) {
        Toast toast = Toast.makeText(getApplicationContext(),
                message,
                Toast.LENGTH_SHORT);
        toast.show();
    }


    /**
     * Riceve il percorso scelto dall'utente nell'activity di scelta della cartella
     *
     * @param requestCode codice con cui è stata avviata l'activity di scelta
     * @param resultCode  specifica se l'operazione è andata a buon fine
     * @param data        intent di risposta che contiene il percorso scelto
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 9999) {
            if (resultCode == RESULT_OK) {
                Uri uri = data.getData();
                Uri path = DocumentsContract.buildDocumentUriUsingTree(
                        uri,
                        DocumentsContract.getTreeDocumentId(uri)
                );
                Log.e("MSG", path.toString());
                selectedPath = getAbsolutePath(this, path);
                if (selectedPath != null) {
                    if (selectedPath.equals(R.string.basicPath)) {
                        txt_pathShow.setText(R.string.basicPath);
                        RadioButton radioButton = findViewById(R.id.radioAllFolders);
                        radioButton.setChecked(true);
                    } else {
                        Log.d("[INFO]", selectedPath);
                        txt_pathShow.setText(selectedPath);
                    }
                }

            }
        }
    }

    /**
     * Metodo per ricavare il percorso assoluto a partire dal percorso relativo
     *
     * @param context stato corrente dell'applicazione
     * @param uri percorso relativo della directory
     * @return ritorna una stringa con il percorso assoluto
     */
    private String getAbsolutePath(Context context, Uri uri) {
        if (DocumentsContract.isDocumentUri(context, uri)) {
            if (isExternalStorageDocument(uri)) {// ExternalStorageProvider
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    if (split.length == 1) {
                        return "/storage/emulated/0";
                    } else {
                        return Environment.getExternalStorageDirectory() + "/" + split[1];
                    }
                }
            }
        }
        return null;
    }

    /**
     * Metodo per verificare che si tratta della radice relativa alla memoria interna
     *
     * @param uri percorso relativo della directory
     * @return true se viene verificata la condizione
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        // If request is cancelled, the result arrays are empty.
        if (requestCode == EXTERNAL_STORAGE_REQ_CODE) {
            if (!(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                showToastMessage(getString(R.string.permission_denied));
                finish();
            }
        }
    }
}
